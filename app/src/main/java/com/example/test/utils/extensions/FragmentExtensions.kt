package com.example.test.utils.extensions

import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.example.test.utils.properties.viewbinding.FragmentBindingProperty

fun <T : ViewBinding> Fragment.viewBinding(viewBinder: (View) -> T): FragmentBindingProperty<T> {
    return FragmentBindingProperty(
        viewBinder
    )
}