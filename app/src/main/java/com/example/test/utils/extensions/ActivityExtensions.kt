package com.example.test.utils.extensions

import android.view.View
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.example.test.utils.properties.viewbinding.ActivityBindingProperty

fun <T : ViewBinding> AppCompatActivity.viewBinding(
    viewBinder: (View) -> T,
    @IdRes viewBindingRootId: Int
): ActivityBindingProperty<T> {
    return ActivityBindingProperty(
        viewBinder,
        viewBindingRootId
    )
}