package com.example.test.utils.properties.viewbinding

import android.view.View
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.viewbinding.ViewBinding
import java.lang.ref.WeakReference
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class FragmentBindingProperty<T : ViewBinding>(
    private val viewBinder: (View) -> T
) : ReadOnlyProperty<Fragment, T> {

    private var viewBinding: T? = null
    private val lifecycleObserver = OnDestroyLifecycleObserver()
    private var fragmentRef: WeakReference<Fragment>? = null

    @MainThread
    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        viewBinding?.let { return it }
        fragmentRef = WeakReference(thisRef)
        thisRef.viewLifecycleOwner.lifecycle.addObserver(lifecycleObserver)
        return viewBinder.invoke(thisRef.requireView()).also {
            viewBinding = it
        }
    }

    private inner class OnDestroyLifecycleObserver : DefaultLifecycleObserver {

        @MainThread
        override fun onDestroy(owner: LifecycleOwner) {
            fragmentRef?.get()?.lifecycle?.removeObserver(lifecycleObserver)
            viewBinding = null
        }
    }
}
