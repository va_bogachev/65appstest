package com.example.test.utils

import java.util.*

class DateHelper {

    companion object{
        private val calendarNow = GregorianCalendar.getInstance()

        fun getAge(birthday: Date): Int{
            val birthdayCalendar: Calendar = Calendar.getInstance(Locale.getDefault()).also {
                it.time = birthday
            }
            var diff = calendarNow.get(Calendar.YEAR) - birthdayCalendar.get(Calendar.YEAR)
            if (birthdayCalendar.get(Calendar.MONTH) > calendarNow.get(Calendar.MONTH) ||
                birthdayCalendar.get(Calendar.MONTH) == calendarNow.get(Calendar.MONTH)
                && birthdayCalendar.get(Calendar.DATE) > calendarNow.get(Calendar.DATE)
            ) {
                diff--
            }
            return diff
        }
    }
}