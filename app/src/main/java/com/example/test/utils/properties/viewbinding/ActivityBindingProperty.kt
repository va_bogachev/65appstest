package com.example.test.utils.properties.viewbinding

import android.view.View
import androidx.activity.ComponentActivity
import androidx.annotation.IdRes
import androidx.annotation.MainThread
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.viewbinding.ViewBinding
import java.lang.ref.WeakReference
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class ActivityBindingProperty<T : ViewBinding>(
    private val viewBinder: (View) -> T,
    @IdRes private val viewBindingRootId: Int
) : ReadOnlyProperty<ComponentActivity, T> {

    private var viewBinding: T? = null
    private val lifecycleObserver = OnDestroyLifecycleObserver()
    private var activityRef: WeakReference<ComponentActivity>? = null

    @MainThread
    override fun getValue(thisRef: ComponentActivity, property: KProperty<*>): T {
        viewBinding?.let { return it }
        activityRef = WeakReference(thisRef)
        thisRef.lifecycle.addObserver(lifecycleObserver)
        return viewBinder.invoke(thisRef.findViewById(viewBindingRootId))
            .also {
                viewBinding = it
            }
    }

    private inner class OnDestroyLifecycleObserver : DefaultLifecycleObserver {

        @MainThread
        override fun onDestroy(owner: LifecycleOwner) {
            activityRef?.get()?.lifecycle?.removeObserver(lifecycleObserver)
            viewBinding = null
        }
    }
}
