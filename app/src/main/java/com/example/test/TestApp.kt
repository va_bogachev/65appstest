package com.example.test

import android.app.Application
import com.example.test.data.repository.di.RepositoryDiHelper
import com.example.test.data.source.network.di.NetworkServiceDiHelper
import com.example.test.data.source.storage.di.LocalStorageDiHelper
import com.example.test.feature.employee_card.di.EmployeeCardFragmentDiHelper
import com.example.test.feature.employee_list.di.EmployeeListFragmentDiHelper
import com.example.test.feature.specialty_list.di.SpecialtyListFragmentDiHelper
import org.koin.core.context.startKoin

class TestApp : Application() {

    private val networkModule = lazy { NetworkServiceDiHelper.getModule() }
    private val localStorageModule = lazy { LocalStorageDiHelper.getModule(applicationContext) }
    private val repositoryModule = lazy { RepositoryDiHelper.getModule() }

    private val featureModules = lazy {
        listOf(
            SpecialtyListFragmentDiHelper.getModule(),
            EmployeeListFragmentDiHelper.getModule(this@TestApp),
            EmployeeCardFragmentDiHelper.getModule(this@TestApp)
        )
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(
                listOf(
                    networkModule.value,
                    localStorageModule.value,
                    repositoryModule.value
                ) +
                        featureModules.value
            )
        }
    }

}