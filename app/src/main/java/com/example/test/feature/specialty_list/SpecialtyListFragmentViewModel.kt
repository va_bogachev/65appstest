package com.example.test.feature.specialty_list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.test.data.repository.IRepository
import com.example.test.data.repository.bo.SpecialtyBO
import io.reactivex.rxjava3.disposables.Disposable

class SpecialtyListFragmentViewModel(
    private val repository: IRepository
) : ViewModel() {

    val specialtyListLiveData = MutableLiveData<List<SpecialtyBO>>()
    val errorLiveData = MutableLiveData<Throwable>()

    private var disposable: Disposable? = null

    init {
        loadData()
    }

    private fun loadData() {
        disposable?.dispose()
        disposable = repository
            .getSpecialtyList()
            .subscribe { res ->
                res.error?.printStackTrace()
                res.data?.let {
                    specialtyListLiveData.postValue(it)
                }
                res.error?.let {
                    it.printStackTrace()
                    errorLiveData.postValue(it)
                }
            }
    }

}