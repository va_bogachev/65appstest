package com.example.test.feature.employee_list.di

import android.content.Context
import com.example.test.feature.employee_list.EmployeeListFragmentViewModel
import com.example.test.feature.employee_list.model.EmployeeUIMapper
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

class EmployeeListFragmentDiHelper {

    companion object {

        fun getModule(context: Context) = module {
            single { EmployeeUIMapper(context) }
            viewModel { (employeeId: Long) ->
                EmployeeListFragmentViewModel(
                    get(),
                    get(),
                    employeeId
                )
            }
        }
    }
}