package com.example.test.feature.employee_card.model

import android.content.Context
import com.example.test.R
import com.example.test.data.repository.bo.EmployeeBO
import com.example.test.utils.DateHelper
import java.text.SimpleDateFormat
import java.util.*

class EmployeeUIMapper(
    private val context: Context
) {

    private val dateFormat =
        SimpleDateFormat(context.getString(R.string.date_format_template), Locale.getDefault())

    fun mapToUIObject(employeeBO: EmployeeBO): EmployeeUIO {
        return EmployeeUIO(
            firstName = employeeBO.firstName,
            lastName = employeeBO.lastName,
            birthdayDate = employeeBO.birthday?.let { dateFormat.format(it) }
                ?: context.getString(R.string.card_employee_no_birthday_or_age),
            age = employeeBO.birthday?.let {
                DateHelper.getAge(it)
            }?.toString() ?: context.getString(R.string.card_employee_no_birthday_or_age),
            avatarUrl = employeeBO.avatarUrl,
            specialtyName = employeeBO.specialty.joinToString(
                separator = ";\n"
            ) { it.name }
        )
    }
}