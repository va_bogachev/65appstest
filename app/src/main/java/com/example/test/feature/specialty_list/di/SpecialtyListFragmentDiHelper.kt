package com.example.test.feature.specialty_list.di

import com.example.test.feature.specialty_list.SpecialtyListFragmentViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

class SpecialtyListFragmentDiHelper {

    companion object {

        fun getModule() = module {
            viewModel { SpecialtyListFragmentViewModel(get()) }
        }
    }
}