package com.example.test.feature.employee_list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.test.data.repository.IRepository
import com.example.test.feature.employee_list.model.EmployeeUIMapper
import com.example.test.feature.employee_list.model.EmployeeUIO
import io.reactivex.rxjava3.disposables.Disposable

class EmployeeListFragmentViewModel(
    private val repository: IRepository,
    private val mapper: EmployeeUIMapper,
    private val specialtyId: Long
) : ViewModel() {

    val employeeListLiveData = MutableLiveData<List<EmployeeUIO>>()
    val errorLiveData = MutableLiveData<Throwable>()

    private var disposable: Disposable? = null

    init {
        loadData()
    }

    private fun loadData() {
        disposable?.dispose()
        disposable = repository
            .getEmployeesBySpecialtyId(specialtyId)
            .subscribe { res ->
                res.error?.printStackTrace()
                res.data?.let { listOfBO ->
                    employeeListLiveData.postValue(
                        listOfBO.map { employeeBO ->
                            mapper.mapToUIObject(employeeBO)
                        }
                    )
                }
                res.error?.let {
                    it.printStackTrace()
                    errorLiveData.postValue(it)
                }
            }
    }
}