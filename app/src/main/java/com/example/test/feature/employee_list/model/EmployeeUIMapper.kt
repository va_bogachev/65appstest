package com.example.test.feature.employee_list.model

import android.content.Context
import com.example.test.R
import com.example.test.data.repository.bo.EmployeeBO
import com.example.test.utils.DateHelper
import java.util.*

class EmployeeUIMapper(private val context: Context) {

    fun mapToUIObject(employeeBO: EmployeeBO): EmployeeUIO {
        return EmployeeUIO(
            id = employeeBO.id,
            firstName = employeeBO.firstName,
            lastName = employeeBO.lastName,
            age = employeeBO.birthday?.let {
                DateHelper.getAge(it)
            }?.toString() ?: context.getString(R.string.item_employee_no_age)
        )
    }
}