package com.example.test.feature.employee_card

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.test.R
import com.example.test.databinding.FragmentEmployeeBinding
import com.example.test.feature.employee_card.model.EmployeeUIO
import com.example.test.utils.extensions.viewBinding
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.io.IOException

class EmployeeCardFragment : Fragment(R.layout.fragment_employee) {

    private val binding by viewBinding(FragmentEmployeeBinding::bind)

    private val viewModel: EmployeeCardFragmentViewModel by viewModel {
        parametersOf(arguments?.getLong(PARAM_EMPLOYEE_ID))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.employeeLiveData.observe(viewLifecycleOwner, Observer { result ->
            bindEmployee(result)
        })
        viewModel.errorLiveData.observe(viewLifecycleOwner, Observer {
            handleError(it)
        })
    }

    private fun bindEmployee(employee: EmployeeUIO) {
        with(binding) {
            firstNameTv.text = employee.firstName
            lastNameTv.text = employee.lastName
            ageTv.text = employee.age
            birthdayTv.text = employee.birthdayDate
            specialtyTv.text = employee.specialtyName
            Glide.with(this@EmployeeCardFragment)
                .load(employee.avatarUrl)
                .placeholder(R.drawable.no_avatar)
                .into(avatarImageView)
        }
    }

    private fun handleError(throwable: Throwable) {
        throwable.printStackTrace()
        when (throwable) {
            is IOException -> {
                showToast(R.string.io_error_title)
            }
            else -> {
                showToast(R.string.unknown_title)
            }
        }
    }

    private fun showToast(@StringRes resId: Int) {
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val PARAM_EMPLOYEE_ID = "com.example.test.param.employee_id"
    }
}