package com.example.test.feature.employee_card.model

data class EmployeeUIO(
    val firstName: String,
    val lastName: String,
    val birthdayDate: String,
    val age: String,
    val avatarUrl: String?,
    val specialtyName: String
)