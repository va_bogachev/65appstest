package com.example.test.feature.employee_list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.test.R
import com.example.test.databinding.ItemEmployeeBinding
import com.example.test.feature.employee_list.model.EmployeeUIO

class EmployeeListAdapter(
    private val onItemClicked: (employeeId: Long) -> Unit
) : RecyclerView.Adapter<EmployeeListAdapter.ViewHolder>() {

    var items: List<EmployeeUIO> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_employee, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding = ItemEmployeeBinding.bind(itemView)

        fun bind(employeeUIO: EmployeeUIO) {
            binding.root.setOnClickListener { onItemClicked.invoke(employeeUIO.id) }
            binding.firstNameTv.text = employeeUIO.firstName
            binding.lastNameTv.text = employeeUIO.lastName
            binding.ageTv.text = employeeUIO.age
        }
    }
}