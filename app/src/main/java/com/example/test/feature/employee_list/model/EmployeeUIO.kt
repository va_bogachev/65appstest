package com.example.test.feature.employee_list.model

data class EmployeeUIO(
    val id: Long,
    val firstName: String,
    val lastName: String,
    val age: String
)