package com.example.test.feature.employee_card.di

import android.content.Context
import com.example.test.feature.employee_card.EmployeeCardFragmentViewModel
import com.example.test.feature.employee_card.model.EmployeeUIMapper
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

class EmployeeCardFragmentDiHelper {

    companion object {

        fun getModule(context: Context) = module {
            single { EmployeeUIMapper(context) }
            viewModel { (employeeId: Long) ->
                EmployeeCardFragmentViewModel(
                    get(),
                    get(),
                    employeeId
                )
            }
        }
    }
}