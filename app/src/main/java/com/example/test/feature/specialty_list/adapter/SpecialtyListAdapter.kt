package com.example.test.feature.specialty_list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.test.R
import com.example.test.data.repository.bo.SpecialtyBO
import com.example.test.databinding.ItemSpecialtyBinding

class SpecialtyListAdapter(
    private val onItemClicked: (specialtyId: Long) -> Unit
) : RecyclerView.Adapter<SpecialtyListAdapter.ViewHolder>() {

    var items: List<SpecialtyBO> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_specialty, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding = ItemSpecialtyBinding.bind(itemView)

        fun bind(specialtyBO: SpecialtyBO) {
            binding.root.setOnClickListener { onItemClicked.invoke(specialtyBO.id) }
            binding.specialtyName.text = specialtyBO.name
        }

    }
}