package com.example.test.feature.employee_card

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.test.data.repository.IRepository
import com.example.test.feature.employee_card.model.EmployeeUIMapper
import com.example.test.feature.employee_card.model.EmployeeUIO
import io.reactivex.rxjava3.disposables.Disposable

class EmployeeCardFragmentViewModel(
    private val repository: IRepository,
    private val mapper: EmployeeUIMapper,
    private val employeeId: Long
) : ViewModel() {

    val employeeLiveData = MutableLiveData<EmployeeUIO>()
    val errorLiveData = MutableLiveData<Throwable>()

    private var disposable: Disposable? = null

    init {
        loadData()
    }

    private fun loadData() {
        disposable?.dispose()
        disposable = repository
            .getEmployeeById(employeeId)
            .subscribe { res ->
                res.error?.printStackTrace()
                res.data?.let { employeeBO ->
                    employeeLiveData.postValue(
                        mapper.mapToUIObject(employeeBO)
                    )
                }
                res.error?.let {
                    it.printStackTrace()
                    errorLiveData.postValue(it)
                }
            }
    }
}