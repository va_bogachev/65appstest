package com.example.test.feature.specialty_list

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.test.R
import com.example.test.Router
import com.example.test.databinding.FragmentListBinding
import com.example.test.feature.specialty_list.adapter.SpecialtyListAdapter
import com.example.test.utils.extensions.viewBinding
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.IOException

class SpecialtyListFragment : Fragment(R.layout.fragment_list) {

    private val binding: FragmentListBinding by viewBinding(FragmentListBinding::bind)
    private val viewModel: SpecialtyListFragmentViewModel by viewModel()
    private val router: Router by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        val adapter = SpecialtyListAdapter(::navigateEmployeeListBySpecialty)
        binding.recyclerView.adapter = adapter

        viewModel.specialtyListLiveData.observe(viewLifecycleOwner, Observer { result ->
            adapter.items = result
        })
        viewModel.errorLiveData.observe(viewLifecycleOwner, Observer {
            handleError(it)
        })
    }

    private fun handleError(throwable: Throwable) {
        throwable.printStackTrace()
        when (throwable) {
            is IOException -> {
                showToast(R.string.io_error_title)
            }
            else -> {
                showToast(R.string.unknown_title)
            }
        }
    }

    private fun navigateEmployeeListBySpecialty(specialtyId: Long) {
        router.navigateEmployeeListBySpecialtyId(specialtyId)
    }

    private fun showToast(@StringRes resId: Int) {
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show()
    }

}