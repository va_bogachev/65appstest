package com.example.test.feature.employee_list

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.test.R
import com.example.test.Router
import com.example.test.databinding.FragmentListBinding
import com.example.test.feature.employee_list.adapter.EmployeeListAdapter
import com.example.test.utils.extensions.viewBinding
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.io.IOException

class EmployeeListFragment : Fragment(R.layout.fragment_list) {

    private val binding by viewBinding(FragmentListBinding::bind)

    private val router: Router by inject()

    private val viewModel: EmployeeListFragmentViewModel by viewModel {
        parametersOf(arguments?.getLong(PARAM_SPECIALTY_ID))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        val adapter = EmployeeListAdapter(::navigateEmployee)
        binding.recyclerView.adapter = adapter

        viewModel.employeeListLiveData.observe(viewLifecycleOwner, Observer { result ->
            adapter.items = result
        })
        viewModel.errorLiveData.observe(viewLifecycleOwner, Observer {
            handleError(it)
        })

    }

    private fun navigateEmployee(employeeId: Long) {
        router.navigateEmployeeCard(employeeId)
    }

    private fun handleError(throwable: Throwable) {
        throwable.printStackTrace()
        when (throwable) {
            is IOException -> {
                showToast(R.string.io_error_title)
            }
            else -> {
                showToast(R.string.unknown_title)
            }
        }
    }

    private fun showToast(@StringRes resId: Int) {
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show()
    }

    companion object {

        const val PARAM_SPECIALTY_ID = "com.example.test.param.specialty_id"
    }

}