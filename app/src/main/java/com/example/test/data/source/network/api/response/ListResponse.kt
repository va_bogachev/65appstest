package com.example.test.data.source.network.api.response

data class ListResponse<T>(
        val response: List<T>?
)