package com.example.test.data.source.network.api.dto

import com.google.gson.annotations.SerializedName

data class SpecialtyDTO(
        @SerializedName("specialty_id")
        val id: Long,
        val name: String
)