package com.example.test.data.source.storage.dbo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "employees")
data class EmployeeDBO(
        val firstName: String,
        val lastName: String,
        val birthday: String?,
        val avatarUrl: String?,
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "employee_id")
        var id: Long = 0
)