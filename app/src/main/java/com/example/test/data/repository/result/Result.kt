package com.example.test.data.repository.result

data class Result<T>(
    val data: T?,
    val error: Throwable?
)