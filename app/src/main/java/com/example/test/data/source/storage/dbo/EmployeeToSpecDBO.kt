package com.example.test.data.source.storage.dbo

import androidx.room.Entity

@Entity(
    tableName = "employee_to_specialty",
    primaryKeys = ["employeeId", "specialtyId"]
)
data class EmployeeToSpecDBO(
    val employeeId: Long,
    val specialtyId: Long
)