package com.example.test.data.source.network

import com.example.test.data.source.network.api.dto.EmployeeDTO
import com.example.test.data.source.network.api.service.RestApi
import com.example.test.data.source.network.exceptions.NoDataException
import io.reactivex.rxjava3.core.Single


class NetworkService(
    private val restApi: RestApi
) : INetworkService {

    override fun getEmployees(): Single<List<EmployeeDTO>> {
        return restApi.getEmployees()
            .map { result ->
                return@map result.response ?: throw NoDataException()
            }
    }
}