package com.example.test.data.source.storage.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.test.data.source.storage.dao.EmployeesDao
import com.example.test.data.source.storage.dbo.EmployeeDBO
import com.example.test.data.source.storage.dbo.EmployeeToSpecDBO
import com.example.test.data.source.storage.dbo.SpecialtyDBO

@Database(
    entities = [
        SpecialtyDBO::class,
        EmployeeDBO::class,
        EmployeeToSpecDBO::class],
    version = 1
)
abstract class EmployeeDatabase : RoomDatabase() {

    abstract fun getDao(): EmployeesDao
}