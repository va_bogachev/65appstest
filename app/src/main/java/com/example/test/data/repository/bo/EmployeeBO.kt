package com.example.test.data.repository.bo

import java.util.*

data class EmployeeBO(
    val id: Long,
    val firstName: String,
    val lastName: String,
    val birthday: Date?,
    val avatarUrl: String?,
    val specialty: List<SpecialtyBO>
)