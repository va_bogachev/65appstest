package com.example.test.data.source.storage.dbo

data class Employee(
    val employeeDBO: EmployeeDBO,
    val specialties: List<SpecialtyDBO>
)