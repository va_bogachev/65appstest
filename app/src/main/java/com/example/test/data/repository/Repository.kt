package com.example.test.data.repository

import com.example.test.data.repository.bo.EmployeeBO
import com.example.test.data.repository.bo.SpecialtyBO
import com.example.test.data.repository.mapper.EmployeeMapper
import com.example.test.data.repository.mapper.SpecialtyMapper
import com.example.test.data.repository.result.Result
import com.example.test.data.source.network.INetworkService
import com.example.test.data.source.storage.ILocalStorage
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class Repository(
    private val localStorage: ILocalStorage,
    private val networkService: INetworkService,
    private val employeeMapper: EmployeeMapper,
    private val specMapper: SpecialtyMapper
) : IRepository {

    override fun getSpecialtyList(): Single<Result<List<SpecialtyBO>>> {
        return loadAndSaveEmployees()
            .subscribeOn(Schedulers.io())
            .toSingleDefault(Result<List<SpecialtyBO>>(null, null))
            .onErrorResumeNext { throwable: Throwable? ->
                return@onErrorResumeNext Single.just(Result<List<SpecialtyBO>>(null, throwable))
            }
            .flatMap { result ->
                localStorage.getSpecialtyList()
                    .map { listDBO -> listDBO.map { specMapper.mapToBO(it) } }
                    .map {
                        return@map result.copy(
                            data = it
                        )
                    }
            }
    }

    override fun getEmployeesBySpecialtyId(specialtyId: Long): Single<Result<List<EmployeeBO>>> {
        return localStorage.getEmployeesBySpecialtyId(specialtyId)
            .subscribeOn(Schedulers.io())
            .map { listDBO -> listDBO.map { DBO -> employeeMapper.mapToBO(DBO) } }
            .map { Result(it, null) }
            .onErrorResumeNext { throwable: Throwable? ->
                return@onErrorResumeNext Single.just(Result<List<EmployeeBO>>(null, throwable))
            }
    }

    override fun getEmployeeById(employeeId: Long): Single<Result<EmployeeBO>> {
        return localStorage.getEmployeeById(employeeId)
            .subscribeOn(Schedulers.io())
            .map { Result(employeeMapper.mapToBO(it), null) }
            .onErrorResumeNext { throwable: Throwable? ->
                return@onErrorResumeNext Single.just(Result<EmployeeBO>(null, throwable))
            }
    }

    private fun loadAndSaveEmployees(): Completable {
        return networkService.getEmployees()
            .flatMapCompletable { listDTO ->
                return@flatMapCompletable localStorage.saveEmployees(
                    listDTO.map { employeeMapper.map(it) }
                )
            }
    }
}