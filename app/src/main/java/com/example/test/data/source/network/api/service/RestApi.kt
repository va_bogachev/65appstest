package com.example.test.data.source.network.api.service

import com.example.test.data.source.network.api.dto.EmployeeDTO
import com.example.test.data.source.network.api.response.ListResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface RestApi {

    @GET("/65gb/static/raw/master/testTask.json")
    fun getEmployees(): Single<ListResponse<EmployeeDTO>>
}