package com.example.test.data.source.storage

import com.example.test.data.source.storage.dbo.Employee
import com.example.test.data.source.storage.dbo.SpecialtyDBO
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface ILocalStorage {

    fun getSpecialtyList(): Single<List<SpecialtyDBO>>

    fun getEmployeesBySpecialtyId(specialtyId: Long): Single<List<Employee>>

    fun getEmployeeById(employeeId: Long): Single<Employee>

    fun saveEmployees(employeeList: List<Employee>): Completable
}