package com.example.test.data.repository.mapper

import android.annotation.SuppressLint
import com.example.test.data.repository.bo.EmployeeBO
import com.example.test.data.source.network.api.dto.EmployeeDTO
import com.example.test.data.source.storage.dbo.Employee
import com.example.test.data.source.storage.dbo.EmployeeDBO
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class EmployeeMapper(
    private val specialtyMapper: SpecialtyMapper
) {

    private val dateFormat1 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    private val dateFormat2 = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())

    @SuppressLint("DefaultLocale")
    fun mapToBO(employee: Employee): EmployeeBO {
        return EmployeeBO(
            id = employee.employeeDBO.id,
            firstName = employee.employeeDBO.firstName.toLowerCase().capitalize(),
            lastName = employee.employeeDBO.lastName.toLowerCase().capitalize(),
            birthday = employee.employeeDBO.birthday?.let { parseDate(it) },
            avatarUrl = employee.employeeDBO.avatarUrl,
            specialty = employee.specialties.map { specialtyMapper.mapToBO(it) }
        )
    }

    fun map(employeeDTO: EmployeeDTO): Employee {
        return Employee(
            employeeDBO = mapToDBO(employeeDTO),
            specialties = employeeDTO.specialty.map { specialtyMapper.mapToDBO(it) }
        )
    }

    private fun mapToDBO(employeeDTO: EmployeeDTO): EmployeeDBO {
        return EmployeeDBO(
            firstName = employeeDTO.firstName,
            lastName = employeeDTO.lastName,
            birthday = employeeDTO.birthday,
            avatarUrl = employeeDTO.avatarUrl
        )
    }


    // КОСТЫЛЬ
    // Данные приходят в разном формате
    // В рамках тестового задания решил обработать этот случай.
    // В реальном проекте при таких услових предпочел бы пойти к бэкам и указать на проблему
    private fun parseDate(string: String): Date? {
        val date1 = try {
            dateFormat1.parse(string)
        } catch (e: ParseException) {
            null
        }
        date1?.let { date ->
            if (string == dateFormat1.format(date)) {
                return date
            }
        }
        val date2 = try {
            dateFormat2.parse(string)
        } catch (e: ParseException) {
            null
        }
        date2?.let { date ->
            if (string == dateFormat2.format(date)) {
                return date
            }
        }
        return null
    }
}