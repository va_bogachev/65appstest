package com.example.test.data.repository.mapper

import com.example.test.data.repository.bo.SpecialtyBO
import com.example.test.data.source.network.api.dto.SpecialtyDTO
import com.example.test.data.source.storage.dbo.SpecialtyDBO

class SpecialtyMapper {

    fun mapToBO(specialtyDBO: SpecialtyDBO): SpecialtyBO {
        return SpecialtyBO(
            id = specialtyDBO.id,
            name = specialtyDBO.name
        )
    }

    fun mapToDBO(specialtyDTO: SpecialtyDTO): SpecialtyDBO {
        return SpecialtyDBO(
            id = specialtyDTO.id,
            name = specialtyDTO.name
        )
    }
}