package com.example.test.data.source.network

import com.example.test.data.source.network.api.dto.EmployeeDTO
import io.reactivex.rxjava3.core.Single

interface INetworkService {

    fun getEmployees(): Single<List<EmployeeDTO>>
}