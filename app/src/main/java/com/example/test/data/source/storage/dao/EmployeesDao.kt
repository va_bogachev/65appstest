package com.example.test.data.source.storage.dao

import androidx.room.*
import com.example.test.data.source.storage.dbo.Employee
import com.example.test.data.source.storage.dbo.EmployeeDBO
import com.example.test.data.source.storage.dbo.EmployeeToSpecDBO
import com.example.test.data.source.storage.dbo.SpecialtyDBO

@Dao
abstract class EmployeesDao {

    @Query("SELECT * FROM specialties")
    abstract fun getSpecialtyList(): List<SpecialtyDBO>

    @Query("SELECT * FROM employees WHERE employee_id == :employeeId")
    abstract fun getEmployeeDBOById(employeeId: Long): EmployeeDBO

    @Query(
        """
            SELECT * FROM specialties JOIN employee_to_specialty 
            WHERE specialties.id == employee_to_specialty.specialtyId 
                AND employee_to_specialty.employeeId == :employeeId
            """
    )
    abstract fun getSpecialtyListByEmployeeId(employeeId: Long): List<SpecialtyDBO>

    @Query(
        """
        SELECT * FROM employees JOIN employee_to_specialty 
        WHERE employee_id == employee_to_specialty.employeeId 
            AND employee_to_specialty.specialtyId == :specialtyId
        """
    )
    abstract fun getEmployeeDBOListSpecialtyId(specialtyId: Long): List<EmployeeDBO>


    @Transaction
    open fun getEmployeeById(employeeId: Long): Employee {
        return Employee(
            employeeDBO = getEmployeeDBOById(employeeId),
            specialties = getSpecialtyListByEmployeeId(employeeId)
        )
    }

    @Transaction
    open fun getEmployeesBySpecialtyId(specialtyId: Long): List<Employee> {
        return getEmployeeDBOListSpecialtyId(specialtyId)
            .map {
                Employee(
                    employeeDBO = it,
                    specialties = getSpecialtyListByEmployeeId(it.id)
                )
            }
    }

    @Transaction
    open fun saveEmployeeList(employeeList: List<Employee>) {
        clearEmployeeToSpecialties()
        clearEmployees()
        val employeesWithId =
            employeeList.zip(insertEmployeeList(employeeList.map { it.employeeDBO }))
                .map { pair ->
                    pair.first.employeeDBO.id = pair.second
                    return@map pair.first
                }
        val employeeToSpecs = employeesWithId.map { employee ->
            employee.specialties.map { specialty ->
                EmployeeToSpecDBO(
                    employee.employeeDBO.id,
                    specialty.id
                )
            }
        }.flatten()
        insertEmployeeToSpecs(employeeToSpecs)
        val specialtyList = employeesWithId
            .map { it.specialties }
            .flatten()
            .distinct()
        insertSpecialtyList(specialtyList)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSpecialtyList(specialtyList: List<SpecialtyDBO>)

    @Insert
    abstract fun insertEmployeeToSpecs(employeeToSpecList: List<EmployeeToSpecDBO>)

    @Insert
    abstract fun insertEmployeeList(employeeList: List<EmployeeDBO>): List<Long>

    @Query("DELETE FROM employee_to_specialty")
    abstract fun clearEmployeeToSpecialties()

    @Query("DELETE FROM employees")
    abstract fun clearEmployees()
}