package com.example.test.data.source.network.api.dto

import com.google.gson.annotations.SerializedName

data class EmployeeDTO(
        @SerializedName("f_name")
        val firstName: String,
        @SerializedName("l_name")
        val lastName: String,
        val birthday: String?,
        @SerializedName("avatr_url")
        val avatarUrl: String?,
        val specialty: List<SpecialtyDTO>
)