package com.example.test.data.source.storage

import com.example.test.data.source.storage.dao.EmployeesDao
import com.example.test.data.source.storage.dbo.Employee
import com.example.test.data.source.storage.dbo.SpecialtyDBO
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.completable.CompletableFromCallable

class LocalStorage(
    private val employeeDao: EmployeesDao
) : ILocalStorage {

    override fun getSpecialtyList(): Single<List<SpecialtyDBO>> {
        return Single.fromCallable { employeeDao.getSpecialtyList() }
    }

    override fun getEmployeesBySpecialtyId(specialtyId: Long): Single<List<Employee>> {
        return Single.fromCallable { employeeDao.getEmployeesBySpecialtyId(specialtyId) }
    }

    override fun getEmployeeById(employeeId: Long): Single<Employee> {
        return Single.fromCallable { employeeDao.getEmployeeById(employeeId) }
    }

    override fun saveEmployees(employeeList: List<Employee>): Completable {
        return CompletableFromCallable { employeeDao.saveEmployeeList(employeeList) }
    }
}