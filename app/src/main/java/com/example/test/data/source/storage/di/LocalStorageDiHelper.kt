package com.example.test.data.source.storage.di

import android.content.Context
import androidx.room.Room
import com.example.test.data.source.storage.ILocalStorage
import com.example.test.data.source.storage.LocalStorage
import com.example.test.data.source.storage.db.EmployeeDatabase
import org.koin.dsl.bind
import org.koin.dsl.module

class LocalStorageDiHelper {

    companion object {

        fun getModule(applicationContext: Context)= module {
            single {
                Room.databaseBuilder(
                    applicationContext,
                    EmployeeDatabase::class.java,
                    "employees_database"
                ).build().getDao()
            }
            single { LocalStorage(get()) } bind ILocalStorage::class
        }
    }
}