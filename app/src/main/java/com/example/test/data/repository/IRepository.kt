package com.example.test.data.repository

import com.example.test.data.repository.bo.EmployeeBO
import com.example.test.data.repository.bo.SpecialtyBO
import com.example.test.data.repository.result.Result
import io.reactivex.rxjava3.core.Single

interface IRepository {

    fun getSpecialtyList(): Single<Result<List<SpecialtyBO>>>

    fun getEmployeesBySpecialtyId(specialtyId: Long) : Single<Result<List<EmployeeBO>>>

    fun getEmployeeById(employeeId: Long): Single<Result<EmployeeBO>>

}