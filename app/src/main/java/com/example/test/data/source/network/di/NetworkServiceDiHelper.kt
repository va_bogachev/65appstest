package com.example.test.data.source.network.di

import com.example.test.BuildConfig
import com.example.test.data.source.network.INetworkService
import com.example.test.data.source.network.NetworkService
import com.example.test.data.source.network.api.service.RestApi
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NetworkServiceDiHelper {

    companion object {

        fun getModule() = module {
            single {
                Retrofit.Builder()
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .addConverterFactory(get())
                    .client(get())
                    .baseUrl("https://gitlab.65apps.com/")
                    .addConverterFactory(get())
                    .build()
                    .create(RestApi::class.java)
            }
            single {
                OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .readTimeout(15, TimeUnit.SECONDS)
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = if (BuildConfig.DEBUG) {
                            HttpLoggingInterceptor.Level.BODY
                        } else {
                            HttpLoggingInterceptor.Level.NONE
                        }
                    })
                    .build()
            }
            single {
                GsonConverterFactory.create(
                    GsonBuilder()
                        .create()
                ) as Converter.Factory
            }
            single { NetworkService(get()) } bind INetworkService::class
        }
    }
}