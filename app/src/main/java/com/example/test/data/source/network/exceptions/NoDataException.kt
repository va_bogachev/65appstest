package com.example.test.data.source.network.exceptions

class NoDataException: Exception("Response is null")