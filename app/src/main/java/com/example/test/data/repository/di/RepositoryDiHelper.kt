package com.example.test.data.repository.di

import com.example.test.data.repository.IRepository
import com.example.test.data.repository.Repository
import com.example.test.data.repository.mapper.EmployeeMapper
import com.example.test.data.repository.mapper.SpecialtyMapper
import org.koin.dsl.bind
import org.koin.dsl.module

class RepositoryDiHelper {

    companion object{

        fun getModule() = module {
            single { SpecialtyMapper() }
            single { EmployeeMapper(get()) }
            single { Repository(get(), get(), get(), get()) } bind IRepository::class
        }
    }
}