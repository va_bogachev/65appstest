package com.example.test.data.repository.bo

data class SpecialtyBO(
    val id: Long,
    val name: String
)