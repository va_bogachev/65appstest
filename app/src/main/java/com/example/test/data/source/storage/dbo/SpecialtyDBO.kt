package com.example.test.data.source.storage.dbo

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "specialties")
data class SpecialtyDBO(
        @PrimaryKey
        val id: Long,
        val name: String
)