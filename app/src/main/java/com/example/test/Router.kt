package com.example.test

import android.os.Bundle
import androidx.navigation.NavController
import com.example.test.feature.employee_card.EmployeeCardFragment
import com.example.test.feature.employee_list.EmployeeListFragment

class Router(
    private val navController: NavController
) {

    fun navigateEmployeeListBySpecialtyId(specialtyId: Long) {
        navController.navigate(R.id.employeeListFragment, Bundle()
            .apply {
                putLong(EmployeeListFragment.PARAM_SPECIALTY_ID, specialtyId)
            }
        )
    }

    fun navigateEmployeeCard(employeeId: Long) {
        navController.navigate(R.id.employeeFragment, Bundle()
            .apply {
                putLong(EmployeeCardFragment.PARAM_EMPLOYEE_ID, employeeId)
            }
        )
    }

}